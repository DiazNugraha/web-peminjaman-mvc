<?php 

class Flasher{
    public static function setBarangFlash($pesan, $aksi, $tipe){
        $_SESSION['barangFlash'] = [
            'pesan' => $pesan,
            'aksi' => $aksi,
            'tipe' => $tipe
        ];
    }

    public static function setFormFlash($pesan, $tipe){
        $_SESSION['formFlash'] = [
            'pesan' => $pesan,
            'tipe' => $tipe
        ];
    }

    public static function setGantiPassFlash($pesan, $aksi, $tipe){
        $_SESSION['gantiPassFlash'] = [
            'pesan' => $pesan,
            'aksi' => $aksi,
            'tipe' => $tipe
        ];
    }

    public static function setFormUserFlash($pesan, $aksi, $tipe){
        $_SESSION['formUserFlash'] = [
            'pesan' => $pesan,
            'aksi' => $aksi,
            'tipe' => $tipe
        ];
    }

    public static function setLoginUserFlash($pesan, $aksi, $tipe){
        $_SESSION['loginUserFlash'] = [
            'pesan' => $pesan,
            'aksi' => $aksi,
            'tipe' => $tipe
        ];
    }

    public static function setLoginAdminFlash($pesan, $aksi, $tipe){
        $_SESSION['loginAdminFlash'] = [
            'pesan' => $pesan,
            'aksi' => $aksi,
            'tipe' => $tipe
        ];
    }

    public static function setGantiPassUserFlash($pesan, $aksi, $tipe){
        $_SESSION['gantiPassUserFlash'] = [
            'pesan' => $pesan,
            'aksi' => $aksi,
            'tipe' => $tipe
        ];
    }

    public static function barangFlash(){
        if (isset($_SESSION['barangFlash'])) {
            echo '<div class="alert alert-' . $_SESSION['barangFlash']['tipe'] .  '" role="alert">
            '  . $_SESSION['barangFlash']['pesan'] . ' ' . $_SESSION['barangFlash']['aksi'] . ' admin
           </div>';
            unset($_SESSION['barangFlash']);
        }
    }

    public static function formFlash(){
        if (isset($_SESSION['formFlash'])) {
            echo '<div class="alert alert-' . $_SESSION['formFlash']['tipe'] .  '" role="alert">
            '  . $_SESSION['formFlash']['pesan'] . ' ' . $_SESSION['formFlash']['aksi'] . ' admin
           </div>';
            unset($_SESSION['formFlash']);
        }
    }

    public static function gantiPassFlash(){
        if (isset($_SESSION['gantiPassFlash'])) {
            echo '<div class="alert alert-' . $_SESSION['gantiPassFlash']['tipe'] .  '" role="alert">
            '  . $_SESSION['gantiPassFlash']['pesan'] . ' ' . $_SESSION['gantiPassFlash']['aksi'] . ' admin
           </div>';
            unset($_SESSION['gantiPassFlash']);
        }
    }

    public static function formUserFlash(){
        if (isset($_SESSION['formUserFlash'])) {
            echo '<div class="alert alert-' . $_SESSION['formUserFlash']['tipe'] .  '" role="alert">
            '  . $_SESSION['formUserFlash']['pesan'] . ' ' . $_SESSION['formUserFlash']['aksi'] . ' Request
           </div>';
            unset($_SESSION['formUserFlash']);
        }
    }

    public static function loginUserFlash(){
        if (isset($_SESSION['loginUserFlash'])) {
            echo '<div class="alert alert-' . $_SESSION['loginUserFlash']['tipe'] .  '" role="alert">
            '  . $_SESSION['loginUserFlash']['pesan'] . ' ' . $_SESSION['loginUserFlash']['aksi'] . ' User
           </div>';
            unset($_SESSION['loginUserFlash']);
        }
    }

    public static function loginAdminFlash(){
        if (isset($_SESSION['loginAdminFlash'])) {          
            echo '<div class="alert alert-' . $_SESSION['loginAdminFlash']['tipe'] .  '" role="alert">
             '  . $_SESSION['loginAdminFlash']['pesan'] . ' ' . $_SESSION['loginAdminFlash']['aksi'] . ' admin
            </div>';
            unset($_SESSION['loginAdminFlash']);
        }
    }

    public static function gantiPassUserFlash(){
        if (isset($_SESSION['gantiPassUserFlash'])) {
            echo '<div class="alert alert-' . $_SESSION['gantiPassUserFlash']['tipe'] .  '" role="alert">
             '  . $_SESSION['gantiPassUserFlash']['pesan'] . ' ' . $_SESSION['gantiPassUserFlash']['aksi'] . ' User
            </div>';
            unset($_SESSION['gantiPassUserFlash']);
        }
    }

}