<?php
class riwayatUser extends Controller{
    public function index(){
        $data['judul'] = 'Riwayat Pinjam';
        $data['user'] = $this->model('User_model')->getDataUserByNim($_SESSION['user']);
        $data['status_peminjam'] = 'active';
        $data['peminjam'] = $this->model('Peminjam_model')->getPeminjamByNim($_SESSION['user']);
        $this->view('user/templates/header', $data);
        $this->view('user/riwayat/index', $data);
        $this->view('user/templates/footer');
    }

    public function details($id){
        $data['judul'] = 'Riwayat Pinjam';
        $data['user'] = $this->model('User_model')->getDataUserByNim($_SESSION['user']);
        $data['status_peminjam'] = 'active';
        $data['peminjam'] = $this->model('Peminjam_model')->getDataPeminjamById($id);
        $this->view('user/templates/header', $data);
        $this->view('user/riwayat/detail', $data);
        $this->view('user/templates/footer');   
    }

}