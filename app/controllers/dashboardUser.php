<?php

class dashboardUser extends Controller{
    public function index(){
        $data['user'] = $this->model('User_model')->getDataUserByNim($_SESSION['user']);
        $data['judul'] = 'Data Barang';        
        $data['status_home'] = 'active';
        $data['barang'] = $this->model('Barang_model')->getAllBarang();
        $this->view('user/templates/header', $data);
        $this->view('user/home', $data);
        $this->view('user/templates/footer');        
    }
}