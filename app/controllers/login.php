<?php 

class login extends Controller{
    public function index(){
        $this->view('login');
    }
    public function signIn(){
        if ( $this->model('User_model')->loginUser($_POST) > 0) {            
            $data['user'] = $this->model('User_model')->getDataUserByNim($_POST['nim_user']);
            $_SESSION['user'] = $data['user']['nim_user'];       
            Flasher::setLoginUserFlash('Berhasil', 'Login', 'success');
            header('Location: ' . BASEURL . '/dashboardUser');
            exit;
        }
        else {
            Flasher::setLoginUserFlash('Gagal', 'Login', 'danger');
            header('Location: ' . BASEURL);
            exit;
        }
    }
}