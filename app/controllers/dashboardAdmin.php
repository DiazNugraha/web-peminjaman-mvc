<?php

class dashboardAdmin extends Controller{
    public function index(){
        $data['admin'] = $this->model('Admin_model')->getDataAdminByUname($_SESSION['admin']);
        $data['judul'] = 'Data Barang';        
        $data['status_home'] = 'active';
        $data['barang'] = $this->model('Barang_model')->getAllBarang();
        $data['mousepad'] = $this->model('Peminjam_model')->getBarangJumlahPinjam("Mouse Pad");        
        $data['mousepad'] = count($data['mousepad']);        
        $data['terminal'] = $this->model('Peminjam_model')->getBarangJumlahPinjam("Terminal");        
        $data['terminal'] = count($data['terminal']);  
        $data['proyektor'] = $this->model('Peminjam_model')->getBarangJumlahPinjam("Proyektor");        
        $data['proyektor'] = count($data['proyektor']);                              
        $data['keyboard'] = $this->model('Peminjam_model')->getBarangJumlahPinjam("Keyboard");                
        $data['keyboard'] = count($data['keyboard']);
        $data['obeng'] = $this->model('Peminjam_model')->getBarangJumlahPinjam("Obeng");        
        $data['obeng'] = count($data['obeng']);  
        $data['cpu'] = $this->model('Peminjam_model')->getBarangJumlahPinjam("cpu");        
        $data['cpu'] = count($data['cpu']);        
        $this->view('admin/templates/header', $data);
        $this->view('admin/home', $data);
        $this->view('admin/templates/footer');
        
    }
}