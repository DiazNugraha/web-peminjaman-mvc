<?php 

class registrasi extends Controller{
    public function index(){
        $this->view('registrasi');   
    }

    public function signUp(){
        $data['user_terdaftar'] = $this->model('User_model')->getAllUser();    
        foreach($data['user_terdaftar'] as $usr){
            if ( $_POST['nim_user'] == $usr['nim_user'] ) {
                header('Location: ' . BASEURL . '/registrasi');
                exit;                
            }
        }        
        if ( $_POST['password_user'] !== $_POST['repassword_user'] ) {            
            header('Location: ' . BASEURL . '/registrasi');
            exit;
        }
        
        if ( $this->model('User_model')->registrasiUser($_POST) > 0) {
            $_SESSION['user'] = $_POST['nim_user'];
            header('Location: ' . BASEURL . '/dashboardUser');
            exit;
        }
        else {
            header('Location: ' . BASEURL . '/registrasi');
            exit;
        }        
    }
}