<?php 

class barangAdmin extends Controller{
    public function index(){
        $data['judul'] = 'Data Barang';
        $data['admin'] = $this->model('Admin_model')->getDataAdminByUname($_SESSION['admin']);
        $data['status_dat_barang'] = 'active';
        $data['barang'] = $this->model('Barang_model')->getAllBarang();
        $this->view('admin/templates/header', $data);
        $this->view('admin/barang/index', $data);
        $this->view('admin/templates/footer');
    }

    public function detail($id){
        $data['judul'] = 'Data Barang';
        $data['nama'] = 'Diaz Nugraha';
        $data['admin'] = $this->model('Admin_model')->getDataAdminByUname($_SESSION['admin']);
        $data['status_dat_barang'] = 'active';
        $data['barang'] = $this->model('Barang_model')->getBarangById($id);
        $this->view('admin/templates/header', $data);
        $this->view('admin/barang/detail', $data);
        $this->view('admin/templates/footer');
    }

    public function getEdit($id){
        $data['judul'] = 'Data Barang';
        $data['nama'] = 'Diaz Nugraha';
        $data['admin'] = $this->model('Admin_model')->getDataAdminByUname($_SESSION['admin']);
        $data['status_dat_barang'] = 'active';
        $data['barang'] = $this->model('Barang_model')->getBarangById($id);
        $this->view('admin/templates/header', $data);
        $this->view('admin/barang/edit', $data);
        $this->view('admin/templates/footer');
    }

    public function edit(){
        if ( $this->model('Barang_model')->editBarang($_POST) > 0 ) {
            Flasher::setBarangFlash('Berhasil', 'Ubah', 'success');
            header('Location: ' . BASEURL . '/barangAdmin');
            exit;
        }
        else {
            Flasher::setBarangFlash('Gagal', 'Ubah', 'danger');
            header('Location: ' . BASEURL . '/barangAdmin');
            exit;
        }
    }

    public function tambah(){
        if ( $this->model('Barang_model')->tambahBarang($_POST) ) {
            Flasher::setBarangFlash('Berhasil', 'Tambah', 'success');
            header('Location: ' . BASEURL . '/barangAdmin');
            exit;
        }
        else {
            Flasher::setBarangFlash('Gagal', 'Tambah', 'danger');
            header('Location: ' . BASEURL . '/barangAdmin');
            exit;
        }
    }

    public function search(){
        $data['admin'] = $this->model('Admin_model')->getDataAdminByUname($_SESSION['admin']);
        $data['judul'] = 'Data Barang';        
        $data['status_dat_barang'] = 'active';
        $data['barang'] = $this->model('Barang_model')->searchBarangByName($_POST['keyword']);
        $this->view('admin/templates/header', $data);
        $this->view('admin/barang/index', $data);
        $this->view('admin/templates/footer');
    }

    public function delete($id){
        if ( $this->model('Barang_model')->deleteBarang($id) > 0) {
            Flasher::setBarangFlash('Berhasil', 'Dihapus', 'success');
            header('Location: ' . BASEURL . '/barangAdmin');
            exit;
        }
        else {
            Flasher::setBarangFlash('Gagal', 'Dihapus', 'danger');
            header('Location: ' . BASEURL . '/barangAdmin');
            exit;
        }
    }

}




?>