<?php

class barangUser extends Controller{
    public function index(){
        $data['user'] = $this->model('User_model')->getDataUserByNim($_SESSION['user']);
        $data['judul'] = 'Data Barang';        
        $data['status_dat_barang'] = 'active';
        $data['barang'] = $this->model('Barang_model')->getAllBarang();
        $this->view('user/templates/header', $data);
        $this->view('user/barang/index', $data);
        $this->view('user/templates/footer');
    }

    public function detail($id){
        $data['user'] = $this->model('User_model')->getDataUserByNim($_SESSION['user']);
        $data['judul'] = 'Data Barang';        
        $data['status_dat_barang'] = 'active';
        $data['barang'] = $this->model('Barang_model')->getBarangById($id);
        $this->view('user/templates/header', $data);
        $this->view('user/barang/detail', $data);
        $this->view('user/templates/footer');
    }

    public function search(){
        $data['user'] = $this->model('User_model')->getDataUserByNim($_SESSION['user']);
        $data['judul'] = 'Data Barang';        
        $data['status_dat_barang'] = 'active';
        $data['barang'] = $this->model('Barang_model')->searchBarangByName($_POST['keyword']);
        $this->view('user/templates/header', $data);
        $this->view('user/barang/index', $data);
        $this->view('user/templates/footer');
    }

}