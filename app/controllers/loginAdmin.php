<?php

class loginAdmin extends Controller{
    public function index(){
        $this->view('loginAdmin');
    }

    public function signIn(){
        if ( $this->model('Admin_model')->loginAdmin($_POST) > 0) {
            Flasher::setLoginAdminFlash('Berhasil', 'Login', 'success');
            $data['admin'] = $this->model('Admin_model')->getDataAdminByUname($_POST['uname']);            
            $_SESSION['admin'] = $data['admin']['uname'];                                    
            header('Location: ' . BASEURL . '/dashboardAdmin');
            exit;
        }
        else {
            Flasher::setLoginAdminFlash('Gagal', 'Login', 'danger');
            header('Location: ' . BASEURL . '/loginAdmin');
            exit;            
        }
    }
}