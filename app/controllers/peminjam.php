<?php 

class peminjam extends Controller{
    public function index(){
        $data['judul'] = 'Peminjam';
        $data['admin'] = $this->model('Admin_model')->getDataAdminByUname($_SESSION['admin']);
        $data['status_peminjam'] = 'active';
        $data['peminjam'] = $this->model('Peminjam_model')->getAllPeminjam();
        $this->view('admin/templates/header', $data);
        $this->view('admin/peminjam/index', $data);
        $this->view('admin/templates/footer');
    }

    public function details($id){
        $data['peminjam'] = $this->model('Peminjam_model')->getDataPeminjamById($id);                
        $data['judul'] = 'Peminjam';
        $data['admin'] = $this->model('Admin_model')->getDataAdminByUname($_SESSION['admin']);
        $data['status_peminjam'] = 'active';
        $this->view('admin/templates/header', $data);
        $this->view('admin/peminjam/detail', $data);
        $this->view('admin/templates/footer');
    }

}



?>