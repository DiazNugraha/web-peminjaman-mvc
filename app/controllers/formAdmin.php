<?php 

class formAdmin extends Controller{
    public function index(){
        $data['judul'] = 'Form Peminjaman';
        $data['admin'] = $this->model('Admin_model')->getDataAdminByUname($_SESSION['admin']);
        $data['status_form'] = 'active';
        $data['barang'] = $this->model('Barang_model')->getAllBarang();
        $this->view('admin/templates/header', $data);
        $this->view('admin/form', $data);
        $this->view('admin/templates/footer');
    }

    public function submitPinjam(){
        $data['barang'] = $this->model('Barang_model')->getBarangByName($_POST);
        $hasil = $data['barang']['jumlah'] - $_POST['jumlahBarang'];
        if ( $hasil < 0) {
                Flasher::setFormFlash('Gagal, jumlah pinjam melebihi batas', 'danger');
                header('Location: ' . BASEURL . '/form');
            exit;
        }
        $tanggalBooking = strtotime($_POST['tanggal_booking']);
        $tanggalPinjam = strtotime($_POST['tanggal_pinjam']);
        $selisih = $tanggalPinjam - $tanggalBooking;
        if ($selisih < 0) {
            Flasher::setFormFlash('Gagal, tanggal sudah terlewat', 'danger');
            header('Location: ' . BASEURL . '/form');
            exit;
        }
        if ( $this->model('Pinjam_model')->pinjam($_POST) > 0 ) {
            if ($this->model('Barang_model')->updateJumlahBarang($hasil, $data['barang']['id']) > 0) {
                Flasher::setFormFlash('Berhasil', 'success');
                header('Location: ' . BASEURL . '/peminjam');
                exit;
            }            
        }else {
            Flasher::setFormFlash('Gagal', 'danger');
            header('Location: ' . BASEURL . '/form');
            exit;
        }                
        
    }
}




?>