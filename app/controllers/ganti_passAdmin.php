<?php 

class ganti_passAdmin extends Controller{
    public function index(){
        $data['judul'] = 'Ganti Password';
        $data['admin'] = $this->model('Admin_model')->getDataAdminByUname($_SESSION['admin']);
        $data['status_ganti_pass'] = 'active';
        $this->view('admin/templates/header', $data);
        $this->view('admin/ganti_pass', $data);
        $this->view('admin/templates/footer');
    }

    public function gantiPass(){
        $data['admin'] = $this->model('Admin_model')->getDataAdminByUname($_SESSION['admin']);        
        $_POST['lama'] = md5($_POST['lama']);        
        $_POST['ulang'] = md5($_POST['ulang']);
        $_POST['baru'] = md5($_POST['baru']);
        if ($_POST['lama'] !== $data['admin']['pass']) {
            Flasher::setGantiPassFlash('Gagal', 'Ganti Password', 'danger');
            header('Location: ' . BASEURL . '/ganti_passAdmin');
            exit;        
        }        
        if ($_POST['baru'] !== $_POST['ulang']) {
            Flasher::setGantiPassFlash('Gagal', 'Ganti Password', 'danger');
            header('Location: ' . BASEURL . '/ganti_passAdmin');
            exit;
        }

        
        
        if ($this->model('Admin_model')->gantiPassAdmin($_POST) > 0) {
            Flasher::setGantiPassFlash('Berhasil', 'Ganti Password', 'success');
            header('Location: ' . BASEURL . '/ganti_passAdmin');
            exit;
        }
        else {
            Flasher::setGantiPassFlash('Gagal', 'Ganti Password', 'danger');
            header('Location: ' . BASEURL . '/ganti_passAdmin');
            exit;
        }
    }

}




?>