<?php

class formUser extends Controller{
    public function index(){
        $data['judul'] = 'Form Peminjaman';
        $data['user'] = $this->model('User_model')->getDataUserByNim($_SESSION['user']);        
        $data['status_form'] = 'active';
        $data['barang'] = $this->model('Barang_model')->getAllBarang();
        $this->view('user/templates/header', $data);
        $this->view('user/form', $data);
        $this->view('user/templates/footer');
    }

    public function submitPinjam(){
        $data['barang'] = $this->model('Barang_model')->getBarangByName($_POST);
        $hasil = $data['barang']['jumlah'] - $_POST['jumlahBarang'];
        if ( $hasil < 0) {
                Flasher::setFormUserFlash('Gagal, jumlah pinjam melebihi batas', 'danger');
                header('Location: ' . BASEURL . '/form');
            exit;
        }
        $tanggalBooking = strtotime($_POST['tanggal_booking']);
        $tanggalPinjam = strtotime($_POST['tanggal_pinjam']);
        $selisih = $tanggalPinjam - $tanggalBooking;
        if ($selisih < 0) {
            Flasher::setFormUserFlash('Gagal, tanggal sudah terlewat', 'danger');
            header('Location: ' . BASEURL . '/form');
            exit;
        }
        if ( $this->model('Pinjam_model')->pinjam($_POST) > 0 ) {
            if ($this->model('Barang_model')->updateJumlahBarang($hasil, $data['barang']['id']) > 0) {
                Flasher::setFormUserFlash('Berhasil', 'Submit' , 'success');
                header('Location: ' . BASEURL . '/formUser');
                exit;
            }            
        }else {
            Flasher::setFormUserFlash('Gagal', 'Submit', 'danger');
            header('Location: ' . BASEURL . '/formUser');
            exit;
        }                       
    }
}