<?php 

class ganti_passUser extends Controller{
    public function index(){
        $data['user'] = $this->model('User_model')->getDataUserByNim($_SESSION['user']);
        $data['judul'] = 'Ganti Password User';
        $data['status_ganti_pass'] = 'active';        
        $this->view('user/templates/header', $data);
        $this->view('user/ganti_pass', $data);
        $this->view('user/templates/footer');
    }

    public function gantiPassword(){
        $data['user'] = $this->model('User_model')->getDataUserByNim($_POST['user']);                
        $lama = md5($_POST['lama']);
        $baru = md5($_POST['baru']);
        $ulang = md5($_POST['ulang']);
        
        if ($lama !== $data['user']['password_user']) {
            Flasher::setGantiPassUserFlash('Gagal', 'Ganti Password', 'danger');
            header('Location: ' . BASEURL . '/ganti_passUser');
            exit;
        }

        if ($baru !== $ulang) {
            Flasher::setGantiPassUserFlash('Gagal', 'Ganti Password', 'danger');
            header('Location: ' . BASEURL . '/ganti_passUser');            
            exit;
        }

        if ( $this->model('User_model')->gantiPasswordUser($baru, $data['user']['id']) ) {
            Flasher::setGantiPassUserFlash('Berhasil', 'Ganti Password', 'success');
            header('Location: ' . BASEURL . '/ganti_passUser');
            exit;
        }else {
            Flasher::setGantiPassUserFlash('Gagal', 'Ganti Password', 'danger');
            header('Location: ' . BASEURL . 'ganti_passUser');
            exit;
        }            
    }

}


?>