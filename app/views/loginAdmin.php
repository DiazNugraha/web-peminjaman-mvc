<!DOCTYPE html>
<html>
<head>
	<title>Website Peminjaman TI</title>
	<link rel="stylesheet" type="text/css" href="<?= BASEURL; ?>/css/bootstrap.css">	
	<link rel="stylesheet" type="text/css" href="<?= BASEURL; ?>/js/jquery-ui/jquery-ui.css">
	<style type="text/css">
	.kotak{	
		margin-top: 150px;
	}

	.kotak .input-group{
		margin-bottom: 20px;
	}
	</style>
</head>
<body>
<!-- <div class="p-3 mb-2 bg-info text-white"> -->
<div class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href="#" class="navbar-brand">Website Peminjaman Barang Maintenance</a>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse">				
				<ul class="nav navbar-nav navbar-right">
					<li><a href="<?= BASEURL; ?>/registrasi" class="">Registrasi</a></li>
					<li><a id="pesan_sedia" href="#" data-toggle="modal" data-target="#modalpesan"><span class='glyphicon glyphicon-comment'></span>  Pesan</a></li>
					<li><a href="<?= BASEURL; ?>/login">Login User &nbsp&nbsp<span class="glyphicon glyphicon-user"></span></a></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="container">
			<div class="row">
				<div class="col-lg-6 mt-4 ml-4">
					<?php Flasher::loginAdminFlash(); ?>  	
				</div>
			</div>
		
			<form action="<?= BASEURL; ?>/loginAdmin/signIn" method="post">
				<div class="col-md-4 col-md-offset-4 kotak">
					<h3>Login Admin</h3>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
						<input type="text" class="form-control" placeholder="Username" name="uname">
					</div>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
						<input type="password" class="form-control" placeholder="Password" name="pass">
					</div>
					<div class="input-group">			
						<input type="submit" class="btn btn-primary" value="Login">
					</div>					
				</div>
			</form>
						
		
	</div>
	<!-- </div> -->
	<script type="text/javascript" src="<?= BASEURL; ?>/js/jquery.js"></script>
        <script src="<?= BASEURL; ?>/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?= BASEURL; ?>/js/jquery-ui/jquery-ui.js"></script>
</body>
</html>