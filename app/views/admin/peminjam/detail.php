
<h3><span class="glyphicon glyphicon-user"></span>  Detail Peminjam</h3>
<a class="btn" href="<?= BASEURL; ?>/peminjam/index"><span class="glyphicon glyphicon-arrow-left"></span>  Kembali</a>



<table class="table">
    <tr>
        <td>Nama</td>
        <td><?= $data['peminjam']['nama']; ?></td>
    </tr>
    <tr>
        <td>NIM</td>
        <td><?= $data['peminjam']['nim']; ?></td>
    </tr>
    <tr>
        <td>Kelas</td>
        <td><?= $data['peminjam']['kelas']; ?></td>
    </tr>
    <tr>
        <td>Jurusan</td>
        <td><?= $data['peminjam']['jurusan']; ?></td>
    </tr>
    <tr>
        <td>Nama Barang yang dipinjam</td>
        <td><?= $data['peminjam']['namaBarang']; ?></td>
    </tr>
    <tr>
        <td>Jumlah Pinjam Barang</td>
        <td><?= $data['peminjam']['jumlahBarang']; ?></td>
    </tr>
    <tr>
        <td>Tanggal Pinjam</td>
        <td><?= $data['peminjam']['tanggalPinjam']; ?></td>
    </tr>

</table>
