<?php Flasher::formFlash(); ?>
<h3><span class="glyphicon glyphicon-user"></span>  Data Peminjaman</h3>
<br/>
<br/>
<form action="#" method="get">
	<div class="input-group col-md-5 col-md-offset-7">
		<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-search"></span></span>
		<input type="text" class="form-control" placeholder="Cari peminjam .." aria-describedby="basic-addon1" name="cari_bio">	
	</div>
</form>
<br/>


<!-- table -->
<table class="table table-hover">
	<tr>
		<th class="col-md-1">No</th>
		<th class="col-md-3">Nama Peminjam</th>
        <th class="col-md-1">NIM</th>
		<th class="col-md-1">Kelas</th>
        <th class="col-md-1">Jurusan</th>
		<th class="col-md-3">Opsi</th>
	</tr>
	<?php 
	$index = 0;
	foreach($data['peminjam'] as $usr) : 
	 	?>
		<tr>
			<td><?= $index =+ 1; ?></td>
			<td><?= $usr['nama']; ?></td>
			<td><?= $usr['nim']; ?></td>
            <td><?= $usr['kelas']; ?></td>
            <td><?= $usr['jurusan']; ?></td>
            
			<td>
				<a href="<?= BASEURL; ?>/peminjam/details/<?php echo $usr['id']; ?>" class="btn btn-info">Detail</a>
				<a href="<?= BASEURL; ?>/peminjam/edit/<?php echo $usr['id']; ?>" class="btn btn-warning">Edit</a>
				<a onclick="if(confirm('Apakah anda yakin ingin menghapus data ini ??')){ location.href='<?= BASEURL; ?>/peminjam/delete/<?= $usr['id']; ?>' }" class="btn btn-danger">Hapus</a>
			</td>
		</tr>		
		<?php 
	endforeach;
	?>
</table>