
<?php Flasher::formFlash(); ?>
<h3><span class="glyphicon glyphicon-briefcase"></span>  Form Peminjaman</h3>
<br/><br/>
<div class="col-md-5 col-md-offset-3">
	<form action="<?= BASEURL; ?>/form/submitPinjam" method="post">
	    <div class="form-group">
			<label>Nama</label>
			<input type="text" name="nama" class="form-control" placeholder="Nama">
		</div>
		<div class="form-group">
			<label>NIM</label>
			<input name="nim" type="text" class="form-control" placeholder="NIM">
		</div>
		<div class="form-group">
			<label>Kelas</label>
			<input name="kelas" type="text" class="form-control" placeholder="Kelas">
		</div>	
        <div class="form-group">
			<label>Jurusan</label>
			<input name="jurusan" type="text" class="form-control" placeholder="Jurusan">
		</div>
        <div class="form-group">
            <label for="inputState">Nama Barang</label>   
            <select name="namaBarang" id="inputState" class="form-control">
            <option selected>Pilih ..</option>
                <?php 
                  foreach($data['barang'] as $brg):
                ?>
				
                <option>
				<?= $brg['nama_barang']; ?>
				</option>
                <?php 
                endforeach;
                ?>
            </select>
        </div>	
        <div class="form-group">
			<label>Jumlah</label>
			<input name="jumlahBarang" type="text" class="form-control" placeholder="Jumlah ..">
		</div>
		<div class="form-group">
				<label for="bookingDate">Tanggal booking</label>
				<input type="text" class="form-control" name="tanggal_booking" id="bookingDate" value="<?= date('Y-m-d'); ?>" readonly>	
		</div>
        <div class="form-group">
            <label>Tanggal</label>
            <input name="tanggal_pinjam" type="date" class="form-control" id="tgl" autocomplete="off" >
        </div>
		<div class="form-group">

			<button type="submit" value="submit" class="btn btn-info">Submit</button>
			<input type="reset" class="btn btn-danger" value="reset">
		</div>																	 
	</form>
</div>