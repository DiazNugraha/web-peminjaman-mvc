<div class="col-md-5 col-md-offset-3">
	<form action="<?= BASEURL; ?>/barangAdmin/edit" method="post">
    <input type="hidden" name="id" value="<?= $data['barang']['id']; ?>">
	    <div class="form-group">
			<label>Nama Barang</label>
			<input name="nama_barang" type="text" class="form-control" value="<?= $data['barang']['nama_barang']; ?>" disabled >
		</div>
		<div class="form-group">
			<label>Jumlah</label>
			<input name="jumlah" type="text" class="form-control" value="<?= $data['barang']['jumlah']; ?>" required >
		</div>
		<div class="form-group">
			<label></label>
			<button type="submit" class="btn btn-info" value="submit">Ubah</button>
			<input type="reset" class="btn btn-danger" value="reset">
		</div>																	 
	</form>
</div>