
            <div class="row">
				<div class="col-lg-6 mt-4 ml-4">
					<?php Flasher::loginAdminFlash(); ?>  	
				</div>
			</div>
<h1>Welcome to dashboard</h1>
<canvas id="myChart" width="300" height="100">adsasl</canvas>
<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Mouse Pad', 'Terminal', 'Proyektor', 'Keyboard', 'Obeng', 'CPU'],
        datasets: [{
            label: 'Grafik Barang Dipinjam',
            data: [<?= $data['mousepad'] ?>, <?= $data['terminal'] ?>, <?= $data['proyektor'] ?>, <?= $data['keyboard'] ?>, <?= $data['obeng'] ?>, <?= $data['cpu'] ?>],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>
