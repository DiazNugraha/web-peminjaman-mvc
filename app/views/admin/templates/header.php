<!DOCTYPE html>
<html>
<head>
	<title>Web Diaz || Admin || <?= $data['judul']; ?></title>
	<link rel="stylesheet" type="text/css" href="<?= BASEURL; ?>/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?= BASEURL; ?>/js/jquery-ui/jquery-ui.css">
	<script type="text/javascript" src="<?= BASEURL; ?>/js/dist/Chart.js"></script>
</head>
<body>
<!-- <div class="container"> -->
	<div class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href="#" class="navbar-brand">My Website</a>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse">				
				<ul class="nav navbar-nav navbar-right">
					<li><a id="pesan_sedia" href="#" data-toggle="modal" data-target="#modalpesan"><span class='glyphicon glyphicon-comment'></span>  Pesan</a></li>
					<li><a class="dropdown-toggle" data-toggle="dropdown" role="button" href="#">Hy , <?php echo $data['admin']['uname'];  ?>&nbsp&nbsp<span class="glyphicon glyphicon-user"></span></a></li>
				</ul>
			</div>
		</div>
	</div>

     <!-- modal input -->

    <div class="col-md-2">

		<div class="row"></div>
		<ul class="nav nav-pills nav-stacked">
			<li class="<?= $data['status_home']; ?>"><a href="<?= BASEURL; ?>/dashboardAdmin"><span class="glyphicon glyphicon-home"></span>  Dashboard</a></li>			
			<li class="<?= $data['status_dat_barang']; ?>"><a href="<?= BASEURL; ?>/barangAdmin"><span class="glyphicon glyphicon-briefcase"></span>  Data Barang</a></li>
			<li class="<?= $data['status_form']; ?>"><a href="<?= BASEURL; ?>/formAdmin"><span class="glyphicon glyphicon-briefcase"></span>  Form Peminjaman</a></li>
			<li class="<?= $data['status_peminjam']; ?>"><a href="<?= BASEURL; ?>/peminjam"><span class="glyphicon glyphicon-briefcase"></span>  Peminjam</a></li>
			<li class="<?= $data['status_ganti_pass']; ?>"><a href="<?= BASEURL; ?>/ganti_passAdmin"><span class="glyphicon glyphicon-lock"></span> Ganti Password</a></li>
			<li><a href="<?= BASEURL; ?>/logout"><span class="glyphicon glyphicon-log-out"></span>  Logout</a></li>			
		</ul>
	</div>
	<div class="col-md-10">
    <!-- <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="<?= BASEURL; ?>">My Website</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="<?= BASEURL; ?>">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= BASEURL; ?>/mahasiswa">mahasiswa</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= BASEURL; ?>/about">About</a>
      </li>
    </ul>
    </div>
  </div>
</nav>
<div class="container mt-3">

<div class="col-md-2">

<div class="row"></div>
<ul class="nav nav-pills nav-stacked">
    <li><a href="index.php"><span class="glyphicon glyphicon-home"></span>  Dashboard</a></li>			
    <li class="active"><a href="barang.php"><span class="glyphicon glyphicon-briefcase"></span>  Data Barang</a></li>
    <li><a href="form.php"><span class="glyphicon glyphicon-briefcase"></span>  Form Peminjaman</a></li>
    <li><a href="peminjam.php"><span class="glyphicon glyphicon-briefcase"></span>  Peminjam</a></li>
    <li><a href="ganti_pass.php"><span class="glyphicon glyphicon-lock"></span> Ganti Password</a></li>
    <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span>  Logout</a></li>			
</ul>
</div>
<div class="col-md-10"> -->

        
