<?php 
Flasher::gantiPassFlash();
?>

<h3><span class="glyphicon glyphicon-briefcase"></span>  Password</h3>
<br/><br/>

<!-- form -->
<br/>
<div class="col-md-5 col-md-offset-3">
	<form action="<?= BASEURL; ?>/ganti_passAdmin/gantiPass" method="post">
		<div class="form-group">
			<input name="id" type="hidden" value="<?= $data['admin']['id']; ?>">
		</div>
		<div class="form-group">
			<label>Password Lama</label>
			<input name="lama" type="password" class="form-control" placeholder="Password Lama ..">
		</div>
		<div class="form-group">
			<label>Password Baru</label>
			<input name="baru" type="password" class="form-control" placeholder="Password Baru ..">
		</div>
		<div class="form-group">
			<label>Ulangi Password</label>
			<input name="ulang" type="password" class="form-control" placeholder="Ulangi Password ..">
		</div>	
		<div class="form-group">			
			<input type="submit" class="btn btn-info" value="submit">
			<input type="reset" class="btn btn-danger" value="reset">
		</div>																	
	</form>
</div>