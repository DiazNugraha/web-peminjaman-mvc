<?php Flasher::formFlash(); 
?>
<h3><span class="glyphicon glyphicon-briefcase"></span>  Data Peminjaman</h3>
<br/>
<br/>
<form action="cari_bio_act.php" method="get">
	<div class="input-group col-md-5 col-md-offset-7">
		<span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-search"></span></span>
		<input type="text" class="form-control" placeholder="Cari peminjam .." aria-describedby="basic-addon1" name="cari_bio">	
	</div>
</form>
<br/>


<!-- table -->
<table class="table table-hover">
	<tr>
		<th class="col-md-1">No</th>
		<th class="col-md-3">Tanggal Booking</th>    
		<th class="col-md-3">Opsi</th>
	</tr>
	<?php 
	$index = 0;
	foreach($data['peminjam'] as $usr) : 
	 	?>
		<tr>
			<td><?= $index =+ 1; ?></td>
			<td><?= $usr['tanggalPinjam']; ?></td>			            
			<td>
				<a href="<?= BASEURL; ?>/riwayatUser/details/<?php echo $usr['id']; ?>" class="btn btn-info">Detail</a>				
			</td>
		</tr>		
		<?php 
	endforeach;
	?>
</table>