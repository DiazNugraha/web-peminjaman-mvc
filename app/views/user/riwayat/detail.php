
<h3><span class="glyphicon glyphicon-briefcase"></span>  Detail Barang</h3>
<a class="btn" href="<?= BASEURL; ?>/riwayatUser"><span class="glyphicon glyphicon-arrow-left"></span>  Kembali</a>



<table class="table">
    <tr>
        <td>Nama</td>
        <td><?= $data['peminjam']['nama']; ?></td>
    </tr>
    <tr>
        <td>NIM</td>
        <td><?= $data['peminjam']['nim']; ?></td>
    </tr>
    <tr>
        <td>Kelas</td>
        <td><?= $data['peminjam']['kelas']; ?></td>
    </tr>
    <tr>
        <td>Jurusan</td>
        <td><?= $data['peminjam']['jurusan']; ?></td>
    </tr>
    <tr>
        <td>Nama Barang Pinjam</td>
        <td><?= $data['peminjam']['namaBarang']; ?></td>
    </tr>
    <tr>
        <td>Jumlah Pinjam</td>
        <td><?= $data['peminjam']['jumlahBarang']; ?></td>
    </tr>
    <tr>
        <td>Tanggal Booking</td>
        <td><?= $data['peminjam']['tanggal_sekarang']; ?></td>
    </tr>
    <tr>
        <td>Tanggal Pinjam</td>
        <td><?= $data['peminjam']['tanggalPinjam']; ?></td>
    </tr>
</table>
