<!DOCTYPE html>
<html>
<head>	
	<title>Web Diaz || User || <?= $data['judul']; ?></title>
	<link rel="stylesheet" type="text/css" href="<?= BASEURL; ?>/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?= BASEURL; ?>/js/jquery-ui/jquery-ui.css">
	<script type="text/javascript" src="<?= BASEURL; ?>/js/jquery.js"></script>
	<script type="text/javascript" src="<?= BASEURL; ?>/js/jquery.js"></script>
	<script type="text/javascript" src="<?= BASEURL; ?>/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?= BASEURL; ?>/js/jquery-ui/jquery-ui.js"></script>	
</head>
<body>
<!-- <div class="container"> -->
	<div class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href="#" class="navbar-brand">My Website</a>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse">				
				<ul class="nav navbar-nav navbar-right">
					<li><a id="pesan_sedia" href="#" data-toggle="modal" data-target="#modalpesan"><span class='glyphicon glyphicon-comment'></span>  Pesan</a></li>
					<li><a class="dropdown-toggle" data-toggle="dropdown" role="button" href="#">Hy , <?php echo $data['user']['nama_user'];  ?>&nbsp&nbsp<span class="glyphicon glyphicon-user"></span></a></li>
				</ul>
			</div>
		</div>
	</div>

     <!-- modal input -->

    <div class="col-md-2">

		<div class="row"></div>
		<ul class="nav nav-pills nav-stacked">
			<li class="<?= $data['status_home']; ?>"><a href="<?= BASEURL; ?>/dashboardUser"><span class="glyphicon glyphicon-home"></span>  Dashboard</a></li>			
			<li class="<?= $data['status_dat_barang']; ?>"><a href="<?= BASEURL; ?>/barangUser"><span class="glyphicon glyphicon-briefcase"></span>  Data Barang</a></li>
			<li class="<?= $data['status_form']; ?>"><a href="<?= BASEURL; ?>/formUser"><span class="glyphicon glyphicon-briefcase"></span>  Form Peminjaman</a></li>
			<li class="<?= $data['status_peminjam']; ?>"><a href="<?= BASEURL; ?>/riwayatUser"><span class="glyphicon glyphicon-briefcase"></span>  Riwayat Peminjaman</a></li>
			<li class="<?= $data['status_ganti_pass']; ?>"><a href="<?= BASEURL; ?>/ganti_passUser"><span class="glyphicon glyphicon-lock"></span> Ganti Password</a></li>
			<li><a href="<?= BASEURL; ?>/logout"><span class="glyphicon glyphicon-log-out"></span>  Logout</a></li>			
		</ul>
	</div>
	<div class="col-md-10">
	<!-- pembatas -->
    