
<div class="row">
    <div class="col-lg-6 mt-4 ml-4">
        <?php Flasher::barangFlash(); ?>  	
    </div>
</div>
<h3><span class="glyphicon glyphicon-briefcase"></span>  Data Barang</h3>
<!-- <button style="margin-bottom:20px" data-toggle="modal" data-target="#myModal" class="btn btn-info col-md-2"><span class="glyphicon glyphicon-plus"></span>Tambah Barang</button> -->
<br/>
<br/>
<form action="<?= BASEURL; ?>/barangUser/search" method="post">
	<div class="input-group col-md-5 col-md-offset-7">		
		<input type="text" class="form-control" placeholder="Cari barang di sini .." aria-describedby="basic-addon1" name="keyword">	
		<span class="input-group-addon" id="basic-addon1"><button style="border-radius:50%;"><span class="glyphicon glyphicon-search"></span></button></span>
	</div>
</form>
<br>

<!-- table -->
<table class="table table-hover">
    <tr>
        <th class="col-md-1">No</th>
        <th class="col-md-4">Nama Barang</th>
        <th class="col-md-1">Jumlah</th>
        <th class="col-md-3">Opsi</th>
    </tr>
    <?php $no = 1;
    foreach($data['barang'] as $b) : 
    ?>
    <tr>
        <td><?= $no++ ?></td>
        <td><?= $b['nama_barang']; ?></td>
        <td><?= $b['jumlah']; ?></td>
        <td>
            <a href="<?= BASEURL; ?>/barangUser/detail/<?= $b['id']; ?>" class="btn btn-info">Detail</a>						
        </td>
    </tr>
    <?php 
    endforeach;
    ?>
</table>



<!-- modal -->
<div id="myModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Tambah Barang Baru</h4>
			</div>
			<div class="modal-body">
				<form action="<?= BASEURL; ?>/barang/tambah" method="post">
					<div class="form-group">
						<label>Nama Barang</label>
						<input name="nama_barang" type="text" class="form-control" placeholder="Nama Barang ..">
					</div>
					<div class="form-group">
						<label>Jumlah</label>
						<input name="jumlah" type="number" class="form-control" placeholder="Jumlah">
					</div>																	

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<button type="submit" value="submit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>