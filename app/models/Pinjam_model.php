<?php 

class Pinjam_model{
    private $table = 'biodata';
    private $db;

    public function __construct(){
        $this->db = new Database;
    }

    public function pinjam($data){
        $query = 'INSERT INTO '  . $this->table . ' VALUES(0, :nama, :nim, :kelas, :jurusan, :namaBarang, :jumlahBarang, :tanggal_booking, :tanggal_pinjam)';
        $this->db->query($query);
        $this->db->bind('nama', $data['nama']);
        $this->db->bind('nim', $data['nim']);
        $this->db->bind('kelas', $data['kelas']);
        $this->db->bind('jurusan', $data['jurusan']);
        $this->db->bind('namaBarang', $data['namaBarang']);
        $this->db->bind('jumlahBarang', $data['jumlahBarang']);
        $this->db->bind('tanggal_booking', $data['tanggal_booking']);
        $this->db->bind('tanggal_pinjam', $data['tanggal_pinjam']);
        $this->db->execute();
        return $this->db->rowCount();
    }

}