<?php

class Peminjam_model{
    private $table = 'biodata';
    private $db;

    public function __construct(){
        $this->db = new Database;
    }

    public function getAllPeminjam(){
        $query = 'SELECT * FROM ' . $this->table;
        $this->db->query($query);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function getPeminjamByNim($nim){
        $query = 'SELECT * FROM ' . $this->table . ' WHERE nim = :nim';
        $this->db->query($query);
        $this->db->bind('nim', $nim);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function getDataPeminjamById($id){
        $query = 'SELECT * FROM ' . $this->table . ' WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('id', $id);
        $this->db->execute();
        return $this->db->single();
    }

    public function getBarangJumlahPinjam($nama_barang){
        $query = 'SELECT * FROM ' . $this->table . ' WHERE namaBarang = :nama_barang';
        $this->db->query($query);
        $this->db->bind('nama_barang', $nama_barang);
        $this->db->execute();
        return $this->db->resultSet();
    }

}