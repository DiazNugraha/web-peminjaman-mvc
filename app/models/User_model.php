<?php 

class User_model{
    private $table = 'user';
    private $db;

    public function __construct(){
        $this->db = new Database;
    }

    public function getAllUser(){
        $query = 'SELECT * FROM ' . $this->table;
        $this->db->query($query);
        return $this->db->resultSet();
    }

    public function registrasiUser($data){        
        $hash = md5($data['password_user']);        
        $query = 'INSERT INTO ' . $this->table . ' VALUES(0, :nama_user, :nim_user, :kelas_user, :jurusan_user, :password_user)';                
        $this->db->query($query);
        $this->db->bind('nama_user', $data['nama_user']);
        $this->db->bind('nim_user', $data['nim_user']);
        $this->db->bind('kelas_user', $data['kelas_user']);
        $this->db->bind('jurusan_user', $data['jurusan_user']);
        $this->db->bind('password_user', $hash);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function loginUser($data){
        $hash = md5($data['password_user']);
        $query = 'SELECT * FROM ' . $this->table . ' WHERE nim_user = :nim_user AND password_user = :password_user';
        $this->db->query($query);
        $this->db->bind('nim_user', $data['nim_user']);
        $this->db->bind('password_user', $hash);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function getDataUserByNim($data){
        $query = 'SELECT * FROM ' . $this->table . ' WHERE nim_user = :nim_user';
        $this->db->query($query);
        $this->db->bind('nim_user', $data);
        $this->db->execute();
        return $this->db->single();
    }

    public function gantiPasswordUser($pass, $id){
        $query = 'UPDATE ' . $this->table . ' SET password_user = :password_user WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('password_user', $pass);
        $this->db->bind('id', $id);
        $this->db->execute();
        return $this->db->rowCount();
    }

}