<?php

class Admin_model{
    private $table = 'admin';
    private $db;

    public function __construct(){
        $this->db = new Database;
    }

    public function loginAdmin($data){
        $hash = md5($data['pass']);
        $query = 'SELECT * FROM ' . $this->table . ' WHERE uname = :uname AND pass = :pass';
        $this->db->query($query);
        $this->db->bind('uname', $data['uname']);
        $this->db->bind('pass', $hash);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function getDataAdminByUname($data){        
        $query = 'SELECT * FROM ' . $this->table . ' WHERE uname = :uname';
        $this->db->query($query);
        $this->db->bind('uname', $data);        
        $this->db->execute();
        return $this->db->single();
    }

    public function gantiPassAdmin($data){
        $query = 'UPDATE ' . $this->table . ' SET pass = :pass WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('pass', $data['baru']);
        $this->db->bind('id', $data['id']);
        $this->db->execute();
        return $this->db->rowCount();        
    }

}