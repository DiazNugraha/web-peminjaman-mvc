<?php 

class Barang_model{

    private $table = 'barang';
    private $db;

    public function __construct(){
        $this->db = new Database;
    }

    public function getAllBarang(){
        $this->db->query('SELECT * FROM ' . $this->table);
        return $this->db->resultSet();
    }

    public function getBarangById($id){
        $this->db->query('SELECT * FROM ' . $this->table . ' WHERE id =' . $id);
        return $this->db->single();
    }

    public function ubahDataBarang($data){
        $this->db->prepare('UPDATE barang WHERE nama_barang=' . $data['nama_barang'] . ', jumlah=' . $data['jumlah'] . 'WHERE id=' . $data['id']);
    }

    public function deleteBarang($id){
        $query = 'DELETE FROM ' . $this->table . ' WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('id', $id);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function tambahBarang($data){
        $query = 'INSERT INTO ' . $this->table . ' VALUES(0, :nama_barang, :jumlah)';
        $this->db->query($query);
        $this->db->bind('nama_barang', $data['nama_barang']);
        $this->db->bind('jumlah', $data['jumlah']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function editBarang($data){
        $query = 'UPDATE ' . $this->table . ' SET jumlah = :jumlah WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('id', $data['id']);
        $this->db->bind('jumlah', $data['jumlah']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function searchBarangByName($keyword){
        $query = "SELECT * FROM barang WHERE nama_barang LIKE '%$keyword%'";
        $this->db->query($query);
        // $this->db->bind('keyword', $keyword);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function getBarangByName($data){
        $query = 'SELECT * FROM ' . $this->table . ' WHERE nama_barang = :namaBarang';
        $this->db->query($query);
        $this->db->bind('namaBarang', $data['namaBarang']);
        $this->db->execute();
        return $this->db->single();
    }

    public function updateJumlahBarang($jumlah, $id){
        $query = 'UPDATE ' . $this->table . ' SET jumlah = :jumlah WHERE id = :id';
        $this->db->query($query);
        $this->db->bind('jumlah', $jumlah);
        $this->db->bind('id', $id);
        $this->db->execute();
        return $this->db->rowCount();
    }

    

}



?>